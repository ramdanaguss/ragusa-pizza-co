import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  cart: [],
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem(state, action) {
      state.cart.push(action.payload);
    },
    deleteItem(state, action) {
      state.cart = state.cart.filter((item) => item.pizzaId !== action.payload);
    },
    increaseItemQuantity(state, action) {
      const item = state.cart.find((item) => item.pizzaId === action.payload);

      item.quantity++;
      item.totalPrice = item.quantity * item.unitPrice;
    },
    decreaseItemQuantity(state, action) {
      const item = state.cart.find((item) => item.pizzaId === action.payload);

      item.quantity--;
      item.totalPrice = item.quantity * item.unitPrice;

      if (item.quantity === 0) {
        cartSlice.caseReducers.deleteItem(state, action);
        return;
      }
    },
    clearItem(state) {
      state.cart = [];
    },
  },
});

export const {
  addItem,
  clearItem,
  decreaseItemQuantity,
  deleteItem,
  increaseItemQuantity,
} = cartSlice.actions;

export function getCart(state) {
  return state.cart.cart;
}

export function getPizzaQuantityById(pizzaId) {
  return (state) =>
    state.cart.cart.find((item) => item.pizzaId === pizzaId)?.quantity ?? 0;
}

export function getTotalQuantity(state) {
  return state.cart.cart.reduce(
    (totalQuantity, item) => totalQuantity + item.quantity,
    0
  );
}

export function getTotalPrice(state) {
  return state.cart.cart.reduce(
    (totalPrice, item) => totalPrice + item.totalPrice,
    0
  );
}

export default cartSlice.reducer;
