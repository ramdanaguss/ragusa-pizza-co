import { useDispatch } from 'react-redux';

import Button from '../../ui/Button';
import { deleteItem } from '../cart/cartSlice';

export default function DeleteItem({ pizzaId }) {
  const dispatch = useDispatch();

  function deleteItemFromCart() {
    dispatch(deleteItem(pizzaId));
  }

  return (
    <Button type="small" onClick={deleteItemFromCart}>
      Delete
    </Button>
  );
}
