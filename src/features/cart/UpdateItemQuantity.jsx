import { useDispatch } from 'react-redux';

import Button from '../../ui/Button';
import { increaseItemQuantity, decreaseItemQuantity } from '../cart/cartSlice';

export default function UpdateItemQuantity({ pizzaId, pizzaQuantity }) {
  const dispatch = useDispatch();

  function increaseQuantity() {
    dispatch(increaseItemQuantity(pizzaId));
  }

  function decreaseQuantity() {
    dispatch(decreaseItemQuantity(pizzaId));
  }
  return (
    <div className="flex items-center gap-2 md:gap-3">
      <Button type="round" onClick={decreaseQuantity}>
        -
      </Button>
      <span>{pizzaQuantity}</span>
      <Button type="round" onClick={increaseQuantity}>
        +
      </Button>
    </div>
  );
}
